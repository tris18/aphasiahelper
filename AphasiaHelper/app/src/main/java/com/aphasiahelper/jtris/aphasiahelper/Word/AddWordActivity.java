package com.aphasiahelper.jtris.aphasiahelper.Word;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.aphasiahelper.jtris.aphasiahelper.Database.DbHelper;
import com.aphasiahelper.jtris.aphasiahelper.MainActivity;
import com.aphasiahelper.jtris.aphasiahelper.R;
import com.aphasiahelper.jtris.aphasiahelper.Utils.Utils;
import com.aphasiahelper.jtris.aphasiahelper.Utils.WordUtils;
import com.squareup.picasso.Picasso;

public class AddWordActivity extends AppCompatActivity {

    private static final int SELECT_PICTURE = 2;
    private static final int TAKE_PICTURE = 4;

    private int id_category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_word);

        final CharSequence[] items = {"Cámara", "Galería", "Sin imagen"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Selecciona una imagen: ");
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // This is the place where you need to execute the logic
                pickImage(items[item].toString());
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

        Intent intent_category = getIntent();
        id_category = intent_category.getIntExtra("id_category", -1);

        Button button = (Button) findViewById(R.id.word_add_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addWord();
            }
        });

    }


    public void pickImage(String option) {
        switch (option) {
            case "Galería":
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");

                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
                break;
            case "Cámara":
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, TAKE_PICTURE);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE || requestCode == TAKE_PICTURE) {

                Bundle extras = data.getExtras();
                if (data.getData() != null)
                    Picasso.get().load(data.getData()).noPlaceholder().centerCrop().fit()
                            .into((ImageView) findViewById(R.id.word_add_image));
                else
                    ((ImageView) findViewById(R.id.word_add_image)).setImageBitmap(((Bitmap) extras.get("data")));
            }
        }
    }


    public String storeImage() {
        String fileName = Long.toString(System.currentTimeMillis()) + ".png";
        ImageView imageView = ((ImageView) findViewById(R.id.word_add_image));

        try {

            Bitmap image = ((BitmapDrawable) imageView.getDrawable()).getBitmap();

            Utils.storeImage(fileName, image, this);
        } catch (java.lang.NullPointerException e) {
            fileName = "empty.png";
        }
        return fileName;
    }

    public void addWord() {
        EditText text = (EditText) findViewById(R.id.word_add_text);
        DbHelper dbHelper = new DbHelper(AddWordActivity.this);

        SQLiteDatabase db;
        db = dbHelper.getReadableDatabase();
        String image = storeImage();
        int id_word = WordUtils.insertWord(db, id_category, text.getText().toString(), image);
        MainActivity.categories.get(id_category).getWords().put(id_word, new Word(image, text.getText().toString(), id_word, id_category));

        db.close();

        Intent intent = new Intent(this, WordsListActivity.class);
        intent.putExtra("id", id_category);
        startActivity(intent);
        finish();
    }

}
