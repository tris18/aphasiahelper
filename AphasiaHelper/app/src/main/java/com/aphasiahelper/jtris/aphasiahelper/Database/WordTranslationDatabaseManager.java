package com.aphasiahelper.jtris.aphasiahelper.Database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by javit_000 on 25/04/2017.
 */

public class WordTranslationDatabaseManager {

    public static final String TABLE_NAME = "word_translation";
    private static final String CN_ID_WORD = "id_word";
    private static final String CN_LANGUAGE = "language";
    private static final String CN_TEXT = "text";
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + CN_ID_WORD + " int ," + CN_LANGUAGE + " varchar(3) NOT NULL," + CN_TEXT + " varchar(255) NOT NULL, PRIMARY KEY (" + CN_LANGUAGE + "," + CN_ID_WORD + "), FOREIGN KEY (" + CN_ID_WORD + ") REFERENCES " + WordDatabaseManager.TABLE_NAME + "(" + WordDatabaseManager.CN_ID + "));";

    public static void insertValue(SQLiteDatabase db, int id, String lang, String text) {
        ContentValues values = new ContentValues();
        values.put(CN_ID_WORD, id);
        values.put(CN_LANGUAGE, lang);
        values.put(CN_TEXT, text);
        db.insert(TABLE_NAME, null, values);
    }

    public static String getText(SQLiteDatabase db, int id_category, String lang) {
        String[] fields = new String[]{CN_TEXT};
        String[] args = new String[]{Integer.toString(id_category), lang};
        Cursor c = db.query(TABLE_NAME, fields, CN_ID_WORD + "=? AND " + CN_LANGUAGE + "=?", args, null, null, null);
        String s = "";
        if (c.moveToFirst())
            s = c.getString(0);
        c.close();
        return s;
    }

    public static void update(SQLiteDatabase db, int id, String lang, String text) {
        ContentValues values = new ContentValues();
        values.put(CN_TEXT, text);
        String[] args = new String[]{Integer.toString(id), lang};
        db.update(TABLE_NAME, values, CN_ID_WORD + "=? AND " + CN_LANGUAGE + "=?", args);

    }

    public static void remove(SQLiteDatabase db, int id, String lang) {
        String[] args = new String[]{Integer.toString(id), lang};
        db.delete(TABLE_NAME, CN_ID_WORD + "=? AND " + CN_LANGUAGE + "=?", args);
    }
}
