package com.aphasiahelper.jtris.aphasiahelper.Category;

import com.aphasiahelper.jtris.aphasiahelper.Word.Word;

import java.util.HashMap;

/**
 * Created by jtris on 24/04/17.
 */

public class Category {
    private String image;
    private String title;
    private int id;
    private int score;
    private HashMap<Integer, Word> words;

    public Category(String image, String title, int id) {
        this.image = image;
        this.title = title;
        this.id = id;
        this.score = 0;
        this.words = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public HashMap<Integer, Word> getWords() {
        return words;
    }

    public void setWords(HashMap<Integer, Word> words) {
        this.words = words;
    }
}
