package com.aphasiahelper.jtris.aphasiahelper.Database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aphasiahelper.jtris.aphasiahelper.Category.Category;
import com.aphasiahelper.jtris.aphasiahelper.MainActivity;

import java.util.ArrayList;

/**
 * Created by javit_000 on 25/04/2017.
 */

public class CategoryDatabaseManager {
    public static final String TABLE_NAME = "Categories";
    public static final String CN_ID = "id";
    public static final String CN_PICTURE = "picture";
    public static final String CN_PARENT_CATEGORY = "parent_category";
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
            + CN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CN_PICTURE + " varchar(255) NOT NULL, "
            + CN_PARENT_CATEGORY + " int NULL, "
            + "FOREIGN KEY (" + CN_PARENT_CATEGORY + ") REFERENCES " + TABLE_NAME + "(" + CN_ID + ")"
            + ");";

    public static final String CREATE_PARENT_CATEGORY_COLUMN = "ALTER TABLE "
            + TABLE_NAME + " ADD COLUMN parent_category int NULL;";


    public static int insertValue(SQLiteDatabase db, String picture) {
        return insertValue(db, picture, null);
    }


    public static int insertValue(SQLiteDatabase db, String picture, Integer parentCategory) {
        ContentValues values = new ContentValues();
        values.put(CN_PICTURE, picture);
        if (parentCategory != null) {
            values.put(CN_PARENT_CATEGORY, parentCategory);
        }
        while (true) {
            return (int) db.insert(TABLE_NAME, null, values);
        }
    }


    public static ArrayList<Category> getAllCategories(SQLiteDatabase db) {
        ArrayList<Category> list = new ArrayList<>();
        Cursor c = db.rawQuery("select * from " + TABLE_NAME, null);
        if (c.moveToFirst())
            do {
                list.add(new Category(c.getString(1), null, c.getInt(0)));
            } while (c.moveToNext());
        c.close();
        return list;
    }

    public static ArrayList<Category> getChildCategories(SQLiteDatabase db, Integer parentCategory) {
        ArrayList<Category> list = new ArrayList<>();
        Cursor c = db.rawQuery("select * from " + TABLE_NAME, null);
        if (c.moveToFirst())
            do {
                list.add(new Category(c.getString(1), null, c.getInt(0)));
            } while (c.moveToNext());
        c.close();
        return list;
    }


    public static void update(SQLiteDatabase db, int id_category, String picture) {
        ContentValues values = new ContentValues();
        values.put(CN_PICTURE, picture);
        String[] args = new String[]{Integer.toString(id_category)};
        db.update(TABLE_NAME, values, CN_ID + "=?", args);
    }

    public static void remove(SQLiteDatabase db, int id) {
        String[] args = new String[]{Integer.toString(id)};
        db.delete(TABLE_NAME, CN_ID + "=?", args);
    }

}