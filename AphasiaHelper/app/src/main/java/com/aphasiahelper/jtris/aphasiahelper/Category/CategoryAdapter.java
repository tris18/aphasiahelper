package com.aphasiahelper.jtris.aphasiahelper.Category;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aphasiahelper.jtris.aphasiahelper.R;
import com.aphasiahelper.jtris.aphasiahelper.Utils.Utils;

import java.util.ArrayList;

/**
 * Created by jtris on 24/04/17.
 */

public class CategoryAdapter extends ArrayAdapter {
    private Context context;
    private int layoutResourceId;
    private ArrayList<Category> data = new ArrayList();

    public CategoryAdapter(Context context, int layoutResourceId, ArrayList data) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.imageTitle = (TextView) row.findViewById(R.id.category_item_text);
            holder.image = (ImageView) row.findViewById(R.id.category_item_image);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        Category item = data.get(position);
        holder.imageTitle.setText(item.getTitle());
        Utils.setImage(holder.image, item.getImage(), context);

        return row;
    }

    static class ViewHolder {
        TextView imageTitle;
        ImageView image;
    }
}
