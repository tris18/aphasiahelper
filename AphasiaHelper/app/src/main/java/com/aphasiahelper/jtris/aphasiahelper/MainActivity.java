package com.aphasiahelper.jtris.aphasiahelper;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.aphasiahelper.jtris.aphasiahelper.Category.AddCategory;
import com.aphasiahelper.jtris.aphasiahelper.Category.Category;
import com.aphasiahelper.jtris.aphasiahelper.Category.CategoryAdapter;
import com.aphasiahelper.jtris.aphasiahelper.Database.DbHelper;
import com.aphasiahelper.jtris.aphasiahelper.Utils.CategoryUtils;
import com.aphasiahelper.jtris.aphasiahelper.Utils.WordUtils;
import com.aphasiahelper.jtris.aphasiahelper.Word.SingleWordActivity;
import com.aphasiahelper.jtris.aphasiahelper.Word.Word;
import com.aphasiahelper.jtris.aphasiahelper.Word.WordAdapter;
import com.aphasiahelper.jtris.aphasiahelper.Word.WordsListActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    public static HashMap<Integer, Category> categories;
    public static ArrayList<Word> recommended;
    private Activity activity;
    private boolean show_fragment = false;

    /*
        Method executed when the application starts. It calls the database to retrieve all the words
        and categories. If the database hasn't been created yet is created. This method also initializes
        the view of the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (recommended == null)
            recommended = new ArrayList<>();

        if (categories == null) {

            DbHelper dbHelper = new DbHelper(this);

            SQLiteDatabase db;
            db = dbHelper.getReadableDatabase();

            categories = CategoryUtils.loadCategories(this, db);

            db.close();
        }

        GridView gridView = (GridView) findViewById(R.id.grid_categories);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_add_category);

        CategoryAdapter gridAdapter = new CategoryAdapter(this, R.layout.category, getData());
        gridView.setAdapter(gridAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectCategory(((Category) parent.getItemAtPosition(position)).getId());
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCategory();
            }
        });


        WordAdapter listAdapter = new WordAdapter(this, R.layout.word, recommended);

        ListView listView = (ListView) findViewById(R.id.recommended_words);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Word word = (Word) parent.getItemAtPosition(position);
                selectWord(word.getId(), word.getImage(), word.getTitle(), word.getId_category());
            }
        });


        activity = this;

    }

    /*
        This method inflates the options menu.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        return true;
    }


    /*
    Method in charge of managing the items selected on the menu. In this case makes visible the recommended
    words layout.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.fragment);
        int id = item.getItemId();
        if (id == R.id.action_recommended) {
            if (show_fragment) {
                linearLayout.setVisibility(View.GONE);
                show_fragment = false;
            } else {
                linearLayout.setVisibility(View.VISIBLE);
                show_fragment = true;
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    Category Listener method. Starts a thread to update the score of the category and opens the category activity.
     */
    public void selectCategory(final int id) {
        Intent intent = new Intent(this, WordsListActivity.class);
        intent.putExtra("id", id);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                DbHelper dbHelper = new DbHelper(activity);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                CategoryUtils.updateCategoryScore(db, id);
                db.close();
            }
        });
        thread.start();
        startActivity(intent);
    }

    /*Listener to the add button. Starts the activity AddCategory*/
    public void addCategory() {
        Intent intent = new Intent(this, AddCategory.class);
        startActivity(intent);
        finish();
    }


    /* Method that gets and sorts the categories*/
    private ArrayList<Category> getData() {

        ArrayList<Category> list = new ArrayList<>();
        list.addAll(categories.values());
        Collections.sort(list, new Comparator<Category>() {
            @Override
            public int compare(Category c1, Category c2) {
                return c2.getScore() - c1.getScore();
            }
        });
        return list;
    }


    /* Listener used to start the word activity and update the score of the word chosen.*/
    public void selectWord(final int id, String picture, String name, final int id_category) {
        Intent intent = new Intent(this, SingleWordActivity.class);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                DbHelper dbHelper = new DbHelper(activity);
                SQLiteDatabase db = dbHelper.getWritableDatabase();

                WordUtils.updateWordScore(db, id_category, id);
                db.close();
            }
        });
        thread.start();

        intent.putExtra("id", id);
        intent.putExtra("picture", picture);
        intent.putExtra("name", name);
        startActivity(intent);
    }
}
