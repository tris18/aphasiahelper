package com.aphasiahelper.jtris.aphasiahelper.Database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aphasiahelper.jtris.aphasiahelper.MainActivity;

/**
 * Created by javit_000 on 25/04/2017.
 */

public class WordDatabaseManager {

    public static final String TABLE_NAME = "Words";
    public static final String CN_ID = "id";
    public static final String CN_PICTURE = "picture";
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
            + CN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CN_PICTURE + " VARCHAR(255) NOT NULL);";

    public static int insertValue(SQLiteDatabase db, String picture) {
        ContentValues values = new ContentValues();
        values.put(CN_PICTURE, picture);

        long generatedId = db.insert(TABLE_NAME, null, values);

        if (generatedId == -1) {
            // Handle the error (e.g., return a special value or throw an exception)
            throw new RuntimeException("Error inserting value into the database");
        }

        return (int) generatedId;
    }

    public static String getImage(SQLiteDatabase db, int id_word) {
        String[] fields = new String[]{CN_PICTURE};
        String[] args = new String[]{Integer.toString(id_word)};
        Cursor c = db.query(TABLE_NAME, fields, CN_ID + "=?", args, null, null, null);
        String s = "";
        if (c.moveToFirst())
            s = c.getString(0);
        c.close();
        return s;
    }

    public static Integer update(SQLiteDatabase db, int id_word, String picture) {
        ContentValues values = new ContentValues();
        values.put(CN_PICTURE, picture);
        String[] args = new String[]{Integer.toString(id_word)};
        int result = db.update(TABLE_NAME, values, CN_ID + "=?", args);
        return result;
    }

    public static void remove(SQLiteDatabase db, int id) {
        String[] args = new String[]{Integer.toString(id)};
        db.delete(TABLE_NAME, CN_ID + "=?", args);
    }

}
