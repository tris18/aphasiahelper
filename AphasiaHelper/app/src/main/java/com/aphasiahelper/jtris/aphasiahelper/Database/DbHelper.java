package com.aphasiahelper.jtris.aphasiahelper.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.aphasiahelper.jtris.aphasiahelper.MainActivity;
import com.aphasiahelper.jtris.aphasiahelper.Utils.CategoryUtils;
import com.aphasiahelper.jtris.aphasiahelper.Utils.WordUtils;

/**
 * Created by javit_000 on 25/04/2017.
 */

public class DbHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "AphasiaHelper.lite";
    public static final int DB_SCHEME_VERSION = 4;

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_SCHEME_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CategoryDatabaseManager.CREATE_TABLE);
        db.execSQL(WordDatabaseManager.CREATE_TABLE);
        db.execSQL(WordCategoryDatabaseManager.CREATE_TABLE);
        db.execSQL(WordTranslationDatabaseManager.CREATE_TABLE);
        db.execSQL(CategoryTranslationDatabaseManager.CREATE_TABLE);
        db.execSQL(WordScoreDatabaseManager.CREATE_TABLE);
        db.execSQL(CategoryScoreDatabaseManager.CREATE_TABLE);
        insertBaseData(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 2) {
            db.execSQL(CategoryDatabaseManager.CREATE_PARENT_CATEGORY_COLUMN);
        }

        if (oldVersion < 3) {

            // Step 1: Rename the original WordDatabaseManager table
            db.execSQL("ALTER TABLE " + WordDatabaseManager.TABLE_NAME + " RENAME TO temp_word_table;");

            // Step 2: Create the new WordDatabaseManager table with AUTOINCREMENT
            db.execSQL("CREATE TABLE " + WordDatabaseManager.TABLE_NAME + " ("
                    + WordDatabaseManager.CN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + WordDatabaseManager.CN_PICTURE + " VARCHAR(255) NOT NULL);");

            // Step 3: Insert existing values into the new table, respecting original IDs
            db.execSQL("INSERT INTO " + WordDatabaseManager.TABLE_NAME + " ("
                    + WordDatabaseManager.CN_ID + ", "
                    + WordDatabaseManager.CN_PICTURE + ") SELECT "
                    + WordDatabaseManager.CN_ID + ", "
                    + WordDatabaseManager.CN_PICTURE + " FROM temp_word_table;");

            // Step 4: Drop the old table
            db.execSQL("DROP TABLE temp_word_table;");
        }
        if (oldVersion < 4) {
            // Step 1: Rename the original Categories table
            db.execSQL("ALTER TABLE " + CategoryDatabaseManager.TABLE_NAME + " RENAME TO temp_categories;");

            // Step 2: Create the new Categories table with AUTOINCREMENT
            db.execSQL("CREATE TABLE " + CategoryDatabaseManager.TABLE_NAME + " ("
                    + CategoryDatabaseManager.CN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + CategoryDatabaseManager.CN_PICTURE + " VARCHAR(255) NOT NULL, "
                    + CategoryDatabaseManager.CN_PARENT_CATEGORY + " INTEGER NULL, "
                    + "FOREIGN KEY (" + CategoryDatabaseManager.CN_PARENT_CATEGORY + ") REFERENCES "
                    + CategoryDatabaseManager.TABLE_NAME + "(" + CategoryDatabaseManager.CN_ID + "));");

            // Step 3: Copy data from the old table to the new table
            db.execSQL("INSERT INTO " + CategoryDatabaseManager.TABLE_NAME + " ("
                    + CategoryDatabaseManager.CN_ID + ", "
                    + CategoryDatabaseManager.CN_PICTURE + ", "
                    + CategoryDatabaseManager.CN_PARENT_CATEGORY + ") SELECT "
                    + CategoryDatabaseManager.CN_ID + ", "
                    + CategoryDatabaseManager.CN_PICTURE + ", "
                    + CategoryDatabaseManager.CN_PARENT_CATEGORY + " FROM temp_categories;");

            // Step 4: Drop the old table
            db.execSQL("DROP TABLE temp_categories;");
        }

    }


    public void insertBaseData(SQLiteDatabase db) {
        int id_category, id_word;

        String[] categories = new String[]{"Ciudades"};

        for (String category : categories) {
            CategoryUtils.insertCategory(db, category, "category1");
        }
        String[] cities = new String[]{"Tarragona", "Barcelona", "Albacete", "Alicante", "Almería", "Ávila", "Badajoz", "Bilbao", "Burgos", "Cáceres", "Cádiz", "Castellón", "Ceuta", "Ciudad Real", "Córdoba", "Cuenca", "Girona", "Granada", "Guadalajara", "Huelva", "Huesca", "Jaén", "La Coruña", "Las Palmas de Gran Canaria", "Lleida", "León", "Logroño", "Lugo", "Madrid", "Málaga", "Melilla", "Murcia", "Orense", "Oviedo", "Palencia", "Palma de Mallorca", "Pamplona", "Pontevedra", "Salamanca", "San Sebastián", "Santa Cruz de Tenerife", "Santander", "Segovia", "Sevilla", "Soria", "Teruel", "Toledo", "Valencia", "Valladolid", "Vitoria", "Zamora", "Zaragoza"};
        int numWords = 0;
        for (String city : cities) {
            numWords = numWords + 1;
            WordUtils.insertWord(db, 1, city, "word" + numWords);
        }
    }
}
