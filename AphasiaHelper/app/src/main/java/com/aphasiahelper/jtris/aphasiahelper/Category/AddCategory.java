package com.aphasiahelper.jtris.aphasiahelper.Category;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.aphasiahelper.jtris.aphasiahelper.Database.DbHelper;
import com.aphasiahelper.jtris.aphasiahelper.MainActivity;
import com.aphasiahelper.jtris.aphasiahelper.R;
import com.aphasiahelper.jtris.aphasiahelper.Utils.CategoryUtils;
import com.aphasiahelper.jtris.aphasiahelper.Utils.Utils;
import com.aphasiahelper.jtris.aphasiahelper.Word.Word;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

public class AddCategory extends AppCompatActivity {

    private static final int SELECT_PICTURE = 1;
    private static final int TAKE_PICTURE = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);

        final CharSequence[] items = {"Cámara", "Galería", "Sin imagen"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Selecciona una imagen: ");
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // This is the place where you need to execute the logic
                pickImage(items[item].toString());
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
        Button button = (Button) findViewById(R.id.category_add_button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCategory();
            }
        });

    }

    public void pickImage(String option) {
        switch (option) {
            case "Galería":
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");

                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
                break;
            case "Cámara":
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, TAKE_PICTURE);
                }
                break;
        }
    }

    public void addCategory() {
        EditText text = (EditText) findViewById(R.id.category_add_text);
        DbHelper dbHelper = new DbHelper(AddCategory.this);

        SQLiteDatabase db;
        db = dbHelper.getReadableDatabase();
        String image = storeImage();
        int id_category = CategoryUtils.insertCategory(db, text.getText().toString(), image);
        MainActivity.categories.put(id_category, new Category(image, text.getText().toString(), id_category));
        MainActivity.categories.get(id_category).setWords(new HashMap<Integer, Word>());
        db.close();

        Intent intent = new Intent(this, MainActivity.class);

        startActivity(intent);
        finish();
    }

    public String storeImage() {
        String fileName = Long.toString(System.currentTimeMillis()) + ".png";
        ImageView imageView = ((ImageView) findViewById(R.id.category_add_image));
        try {

            Bitmap image = ((BitmapDrawable) imageView.getDrawable()).getBitmap();


            Utils.storeImage(fileName, image, this);
        } catch (java.lang.NullPointerException e) {
            fileName = "empty.png";
        }
        return fileName;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE || requestCode == TAKE_PICTURE) {

                Bundle extras = data.getExtras();
                if (data.getData() != null)
                    Picasso.get().load(data.getData()).noPlaceholder().centerCrop().fit()
                            .into((ImageView) findViewById(R.id.category_add_image));
                else
                    ((ImageView) findViewById(R.id.category_add_image)).setImageBitmap(((Bitmap) extras.get("data")));
            }

        }
    }
}
