package com.aphasiahelper.jtris.aphasiahelper.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;

import com.aphasiahelper.jtris.aphasiahelper.Category.Category;
import com.aphasiahelper.jtris.aphasiahelper.Database.CategoryDatabaseManager;
import com.aphasiahelper.jtris.aphasiahelper.Database.CategoryScoreDatabaseManager;
import com.aphasiahelper.jtris.aphasiahelper.Database.CategoryTranslationDatabaseManager;
import com.aphasiahelper.jtris.aphasiahelper.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by javit_000 on 03/06/2017.
 */

public class CategoryUtils {
    public static int insertCategory(SQLiteDatabase db, String name, String image) {
        int id_category = CategoryDatabaseManager.insertValue(db, image);
        CategoryTranslationDatabaseManager.insertValue(db, id_category, "es", name);
        CategoryScoreDatabaseManager.insertScore(db, id_category);
        return id_category;
    }

    public static Category getCategory(SQLiteDatabase db, int id_category, Resources resources) {
        String text = CategoryTranslationDatabaseManager.getText(db, id_category, "es");
        Category category = new Category(null, text, id_category);
        return category;
    }

    public static ArrayList<Category> getAllCategories(SQLiteDatabase db) {
        ArrayList<Category> list = CategoryDatabaseManager.getAllCategories(db);
        for (Category c : list) {

        }
        return list;
    }

    public static HashMap<Integer, Category> loadCategories(Context context, SQLiteDatabase db) {
        ArrayList<Category> list = CategoryDatabaseManager.getAllCategories(db);
        HashMap<Integer, Category> map = new HashMap<>();
        for (Category c : list) {
            c.setTitle(CategoryTranslationDatabaseManager.getText(db, c.getId(), "es"));
            c.setScore(CategoryScoreDatabaseManager.getScore(db, c.getId()));
            c.setWords(WordUtils.loadCategoryWords(context, db, c.getId()));
            map.put(c.getId(), c);
        }

        return map;
    }

    public static HashMap<Integer, Category> loadChildCategories(Context context, SQLiteDatabase db, Integer parentCategory) {
        ArrayList<Category> list = CategoryDatabaseManager.getChildCategories(db, parentCategory);
        HashMap<Integer, Category> map = new HashMap<>();
        for (Category c : list) {
            c.setTitle(CategoryTranslationDatabaseManager.getText(db, c.getId(), "es"));
            c.setScore(CategoryScoreDatabaseManager.getScore(db, c.getId()));
            c.setWords(WordUtils.loadCategoryWords(context, db, c.getId()));
            map.put(c.getId(), c);
        }

        return map;
    }

    public static void updateCategoryScore(SQLiteDatabase db, int id_category) {
        Category category = MainActivity.categories.get(id_category);
        int score = category.getScore() + 1;
        category.setScore(score);
        CategoryScoreDatabaseManager.updateScore(db, id_category, score);
    }

    public static void removeCategory(SQLiteDatabase db, int id_category) {
        Set<Integer> words = MainActivity.categories.get(id_category).getWords().keySet();

        Set<Integer> wordsCopy = new HashSet<>(words);
        for (Integer id_word : wordsCopy) {
            try {
                WordUtils.removeWord(db, id_word, id_category);
            } catch (Exception e) {
                continue;
            }
        }

        MainActivity.categories.remove(id_category);
        CategoryTranslationDatabaseManager.remove(db, id_category, "es");
        CategoryScoreDatabaseManager.remove(db, id_category);
        CategoryDatabaseManager.remove(db, id_category);
    }

    public static void updateCategory(SQLiteDatabase db, int id_category, String picture, String text) {
        MainActivity.categories.get(id_category).setTitle(text);
        MainActivity.categories.get(id_category).setImage(picture);
        CategoryDatabaseManager.update(db, id_category, picture);
        CategoryTranslationDatabaseManager.update(db, id_category, text, "es");
    }
}
