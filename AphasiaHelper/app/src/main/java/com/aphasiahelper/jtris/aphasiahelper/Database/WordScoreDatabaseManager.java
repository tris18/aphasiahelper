package com.aphasiahelper.jtris.aphasiahelper.Database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aphasiahelper.jtris.aphasiahelper.Word.Word;

import java.util.Calendar;


/**
 * Created by javit_000 on 25/04/2017.
 */

public class WordScoreDatabaseManager {
    public static final String TABLE_NAME = "word_scores";
    private static final String CN_ID_WORD = "id_word";
    private static final String CN_SCORE = "score";
    private static final String CN_MORNING_MULTIPLIER = "morning_multiplier";
    private static final String CN_MIDDAY_MULTIPLIER = "midday_multiplier";
    private static final String CN_AFTERNOON_MULTIPLIER = "afternoon_multiplier";
    private static final String CN_EVENING_MULTIPLIER = "evening_multiplier";
    private static final String CN_NIGHT_MULTIPLIER = "night_multiplier";
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + CN_ID_WORD + " int ," + CN_SCORE + " int NOT NULL," + CN_MORNING_MULTIPLIER + " FLOAT NOT NULL," + CN_MIDDAY_MULTIPLIER + " FLOAT NOT NULL, " + CN_AFTERNOON_MULTIPLIER + " FLOAT NOT NULL," + CN_EVENING_MULTIPLIER + " FLOAT NOT NULL," + CN_NIGHT_MULTIPLIER + " FLOAT NOT NULL, PRIMARY KEY (" + CN_ID_WORD + "), FOREIGN KEY (" + CN_ID_WORD + ") REFERENCES " + WordDatabaseManager.TABLE_NAME + "(" + WordDatabaseManager.CN_ID + "));";


    public static void insertValue(SQLiteDatabase db, int id) {
        ContentValues values = new ContentValues();
        values.put(CN_ID_WORD, id);
        values.put(CN_SCORE, 0);
        values.put(CN_MORNING_MULTIPLIER, 1);
        values.put(CN_MIDDAY_MULTIPLIER, 1);
        values.put(CN_AFTERNOON_MULTIPLIER, 1);
        values.put(CN_EVENING_MULTIPLIER, 1);
        values.put(CN_NIGHT_MULTIPLIER, 1);
        db.insert(TABLE_NAME, null, values);
    }

    public static Word getMultipliers(SQLiteDatabase db, Word word) {
        String[] col = new String[]{CN_MORNING_MULTIPLIER, CN_MIDDAY_MULTIPLIER, CN_AFTERNOON_MULTIPLIER, CN_EVENING_MULTIPLIER, CN_NIGHT_MULTIPLIER};
        String[] args = new String[]{Integer.toString(word.getId())};
        Cursor c = db.query(TABLE_NAME, col, CN_ID_WORD + "=?", args, null, null, null);

        if (c.moveToFirst()) {
            word.setMorningMultiplier(c.getDouble(0));
            word.setMiddayMultiplier(c.getDouble(1));
            word.setAfternoonMultiplier(c.getDouble(2));
            word.setEveningMultiplier(c.getDouble(3));
            word.setNightMultiplier(c.getDouble(4));
        }

        c.close();
        return word;
    }

    public static int getScore(SQLiteDatabase db, int id_word) {
        String[] col = new String[]{CN_SCORE};
        String[] args = new String[]{Integer.toString(id_word)};
        int score = 0;
        Cursor c = db.query(TABLE_NAME, col, CN_ID_WORD + "=?", args, null, null, null);
        if (c.moveToFirst())
            score = c.getInt(0);
        c.close();
        return score;
    }

    public static void updateScore(SQLiteDatabase db, int id_word) {

        int score = getScore(db, id_word);
        score++;
        ContentValues values = new ContentValues();
        String[] args = new String[]{Integer.toString(id_word)};
        values.put(CN_SCORE, score);
        db.update(TABLE_NAME, values, CN_ID_WORD + "=?", args);

        updateMultiplier(db, id_word);
    }

    private static void updateMultiplier(SQLiteDatabase db, int id_word) {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        String s;

        if (hour <= 0 && hour < 6)
            s = CN_NIGHT_MULTIPLIER;
        else if (hour < 12)
            s = CN_MORNING_MULTIPLIER;
        else if (hour < 15)
            s = CN_MIDDAY_MULTIPLIER;
        else if (hour < 19)
            s = CN_AFTERNOON_MULTIPLIER;
        else
            s = CN_EVENING_MULTIPLIER;

        String[] col = new String[]{s};
        String[] args = new String[]{Integer.toString(id_word)};

        Cursor c = db.query(TABLE_NAME, col, CN_ID_WORD + "=?", args, null, null, null);
        double multiplier = 0.1;
        if (c.moveToFirst())
            multiplier = c.getFloat(0);
        c.close();

        ContentValues values = new ContentValues();
        values.put(s, multiplier);
        db.update(TABLE_NAME, values, CN_ID_WORD + "=?", args);

    }

    public static void remove(SQLiteDatabase db, int id) {
        String[] args = new String[]{Integer.toString(id)};
        db.delete(TABLE_NAME, CN_ID_WORD + "=?", args);
    }
}
