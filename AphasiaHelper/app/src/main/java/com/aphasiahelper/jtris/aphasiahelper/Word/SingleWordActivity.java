package com.aphasiahelper.jtris.aphasiahelper.Word;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aphasiahelper.jtris.aphasiahelper.Database.DbHelper;
import com.aphasiahelper.jtris.aphasiahelper.MainActivity;
import com.aphasiahelper.jtris.aphasiahelper.R;
import com.aphasiahelper.jtris.aphasiahelper.Utils.Utils;
import com.aphasiahelper.jtris.aphasiahelper.Utils.WordUtils;

import java.util.Locale;

public class SingleWordActivity extends AppCompatActivity {

    private String picture;
    private String text;
    private int id_word, id_category;
    private Activity activity;
    private TextToSpeech textToSpeech;
    private Button playButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_word);
        Intent intent = getIntent();
        picture = intent.getStringExtra("picture");
        text = intent.getStringExtra("name");
        id_word = intent.getIntExtra("id", -1);
        id_category = intent.getIntExtra("id_category", -1);
        ImageView imageView = (ImageView) findViewById(R.id.single_word_image);
        TextView textView = (TextView) findViewById(R.id.sigle_word_text);
        activity = this;
        Utils.setImage(imageView, picture, this);
        textView.setText(text);

        playButton = findViewById(R.id.playButton);

        textToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = textToSpeech.setLanguage(new Locale("es", "ES")); // Español de España

                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        // Manejar el error
                        Log.e("TTS", "Idioma no soportado");
                    }
                } else {
                    Log.e("TTS", "Inicialización fallida");
                }
            }
        });

        playButton.setOnClickListener(v -> {

            if (!text.isEmpty()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
                } else {
                    Toast.makeText(this, "Lo siento, tu versión de Android no es compatible con esta funcionalidad.", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    /*
      This method inflates the options menu.
   */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.editremove, menu);
        return true;
    }


    /*
    Method in charge of managing the items selected on the menu.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_edit) {
            Intent intent = new Intent(this, EditWord.class);
            intent.putExtra("id_word", id_word);
            intent.putExtra("id_category", id_category);
            startActivity(intent);
            this.finish();

        } else if (id == R.id.action_remove) {

            new AlertDialog.Builder(this)
                    .setTitle("Elminar palabra")
                    .setMessage("¿Estás seguro de querer eliminar la  palabra " + MainActivity.categories.get(id_category).getWords().get(id_word).getTitle() + "?")
                    .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DbHelper dbHelper = new DbHelper(activity);
                            SQLiteDatabase db = dbHelper.getWritableDatabase();
                            WordUtils.removeWord(db, id_word, id_category);
                            db.close();

                            Intent intent = new Intent(activity, WordsListActivity.class);
                            intent.putExtra("id", id_category);
                            startActivity(intent);

                            activity.finish();
                        }
                    }).setNegativeButton("No", null).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }
}
