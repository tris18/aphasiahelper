package com.aphasiahelper.jtris.aphasiahelper.Database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by javit_000 on 25/04/2017.
 */

public class CategoryScoreDatabaseManager {
    private static final String TABLE_NAME = "CategoryScore";
    private static final String CN_ID_CATEGORY = "id_category";
    private static final String CN_SCORE = "score";
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + CN_ID_CATEGORY + " int ," + CN_SCORE + " int NOT NULL, PRIMARY KEY (" + CN_ID_CATEGORY + "), FOREIGN KEY (" + CN_ID_CATEGORY + ") REFERENCES " + WordDatabaseManager.TABLE_NAME + "(" + WordDatabaseManager.CN_ID + "));";


    public static void insertScore(SQLiteDatabase db, int id) {
        ContentValues values = new ContentValues();
        values.put(CN_ID_CATEGORY, id);
        values.put(CN_SCORE, 0);
        db.insert(TABLE_NAME, null, values);
    }

    public static int getScore(SQLiteDatabase db, int id_category) {
        String[] cols = new String[]{CN_SCORE};
        String[] args = new String[]{Integer.toString(id_category)};
        Cursor c = db.query(TABLE_NAME, cols, CN_ID_CATEGORY + "=?", args, null, null, null);

        int score = 0;
        if (c.moveToFirst())
            score = c.getInt(0);
        c.close();
        return score;
    }

    public static void updateScore(SQLiteDatabase db, int id_category, int score) {
        ContentValues values = new ContentValues();
        String[] args = new String[]{Integer.toString(id_category)};
        values.put(CN_SCORE, score);
        db.update(TABLE_NAME, values, CN_ID_CATEGORY + "=?", args);
    }

    public static void remove(SQLiteDatabase db, int id) {
        String[] args = new String[]{Integer.toString(id)};
        db.delete(TABLE_NAME, CN_ID_CATEGORY + "=?", args);
    }
}
