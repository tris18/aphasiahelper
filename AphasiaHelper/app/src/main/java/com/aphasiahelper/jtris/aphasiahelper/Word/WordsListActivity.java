package com.aphasiahelper.jtris.aphasiahelper.Word;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.aphasiahelper.jtris.aphasiahelper.Category.EditCategory;
import com.aphasiahelper.jtris.aphasiahelper.Database.DbHelper;
import com.aphasiahelper.jtris.aphasiahelper.MainActivity;
import com.aphasiahelper.jtris.aphasiahelper.R;
import com.aphasiahelper.jtris.aphasiahelper.Utils.CategoryUtils;
import com.aphasiahelper.jtris.aphasiahelper.Utils.WordUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class WordsListActivity extends AppCompatActivity {

    private int id_category;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        id_category = getIntent().getIntExtra("id", 0);
        setContentView(R.layout.activity_words_list);

        ListView listView = (ListView) findViewById(R.id.list_words);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_add_word);

        WordAdapter listAdapter = new WordAdapter(this, R.layout.word, getData());
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Word word = (Word) parent.getItemAtPosition(position);
                selectWord(word.getId(), word.getImage(), word.getTitle());
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addWord();
            }
        });
        activity = this;
    }

    /*
       This method inflates the options menu.
    */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.editremove, menu);
        return true;
    }


    /*
    Method in charge of managing the items selected on the menu.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_edit) {
            Intent intent = new Intent(this, EditCategory.class);
            intent.putExtra("id_category", id_category);
            startActivity(intent);
            this.finish();
        } else if (id == R.id.action_remove) {

            new AlertDialog.Builder(this)
                    .setTitle("Elminar categoría")
                    .setMessage("¿Estás seguro de querer eliminar la categoría " + MainActivity.categories.get(id_category).getTitle() + "?")
                    .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DbHelper dbHelper = new DbHelper(activity);
                            SQLiteDatabase db = dbHelper.getWritableDatabase();
                            CategoryUtils.removeCategory(db, id_category);
                            db.close();

                            Intent intent = new Intent(activity, MainActivity.class);
                            startActivity(intent);
                            activity.finish();
                        }
                    }).setNegativeButton("No", null).show();
        }

        return super.onOptionsItemSelected(item);
    }

    public void selectWord(final int id, String picture, String name) {
        Intent intent = new Intent(this, SingleWordActivity.class);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                DbHelper dbHelper = new DbHelper(activity);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                WordUtils.updateWordScore(db, id_category, id);
                db.close();
            }
        });
        thread.start();

        intent.putExtra("id", id);
        intent.putExtra("picture", picture);
        intent.putExtra("name", name);
        intent.putExtra("id_category", id_category);
        startActivity(intent);

        this.finish();
    }

    public void addWord() {
        Intent intent = new Intent(this, AddWordActivity.class);
        intent.putExtra("id_category", id_category);
        startActivity(intent);
        finish();
    }

    public ArrayList<Word> getData() {
        HashMap<Integer, Word> words = MainActivity.categories.get(id_category).getWords();
        ArrayList<Word> list = new ArrayList<>();
        list.addAll(words.values());

        Collections.sort(list, new Comparator<Word>() {
            @Override
            public int compare(Word o1, Word o2) {
                return (int) (o2.getScore() - o1.getScore());
            }
        });

        return list;
    }
}
