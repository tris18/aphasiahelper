package com.aphasiahelper.jtris.aphasiahelper.Word;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aphasiahelper.jtris.aphasiahelper.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class WordFragment extends Fragment {


    public WordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_word, container, false);
    }

}
