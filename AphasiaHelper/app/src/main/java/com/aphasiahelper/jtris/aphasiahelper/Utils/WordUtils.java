package com.aphasiahelper.jtris.aphasiahelper.Utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.aphasiahelper.jtris.aphasiahelper.Category.Category;
import com.aphasiahelper.jtris.aphasiahelper.Database.WordCategoryDatabaseManager;
import com.aphasiahelper.jtris.aphasiahelper.Database.WordDatabaseManager;
import com.aphasiahelper.jtris.aphasiahelper.Database.WordScoreDatabaseManager;
import com.aphasiahelper.jtris.aphasiahelper.Database.WordTranslationDatabaseManager;
import com.aphasiahelper.jtris.aphasiahelper.MainActivity;
import com.aphasiahelper.jtris.aphasiahelper.Word.Word;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by javit_000 on 03/06/2017.
 */

public class WordUtils {
    public static int insertWord(SQLiteDatabase db, int id_category, String name, String image_name) {

        int id_word = WordDatabaseManager.insertValue(db, image_name);
        WordCategoryDatabaseManager.insertValue(db, id_category, id_word);
        WordScoreDatabaseManager.insertValue(db, id_word);
        WordTranslationDatabaseManager.insertValue(db, id_word, "es", name);
        return id_word;
    }

    static void insertRecommended(Word word) {
        if (MainActivity.recommended.size() == 0) {
            MainActivity.recommended.add(word);
        } else {
            int i = 0;
            while (MainActivity.recommended.size() > i && MainActivity.recommended.get(i).getScore() > word.getScore()) {
                i++;
            }
            MainActivity.recommended.add(i, word);

            if (MainActivity.recommended.size() > 3)
                MainActivity.recommended.remove(3);
        }
    }

    public static void updateWordScore(SQLiteDatabase db, int id_category, int id_word) {
        Category category = MainActivity.categories.get(id_category);
        Word word = category.getWords().get(id_word);
        word.updateScore();
        word.updateMultiplier();
        WordScoreDatabaseManager.updateScore(db, id_word);
    }

    public static HashMap<Integer, Word> loadCategoryWords(Context context, SQLiteDatabase db, int id_category) {
        ArrayList<Word> list = WordCategoryDatabaseManager.getWordsByCategory(db, id_category);
        HashMap<Integer, Word> map = new HashMap<>();
        for (Word w : list) {
            w.setImage(WordDatabaseManager.getImage(db, w.getId()));
            w.setTitle(WordTranslationDatabaseManager.getText(db, w.getId(), "es"));
            w.setScore(WordScoreDatabaseManager.getScore(db, w.getId()));
            w.setId_category(id_category);
            w = WordScoreDatabaseManager.getMultipliers(db, w);
            map.put(w.getId(), w);

            insertRecommended(w);
        }
        return map;
    }

    public static void removeWord(SQLiteDatabase db, int id_word, int id_category) {
        MainActivity.categories.get(id_category).getWords().remove(id_word);
        WordScoreDatabaseManager.remove(db, id_word);
        WordTranslationDatabaseManager.remove(db, id_word, "es");
        WordCategoryDatabaseManager.removeWord(db, id_word);
        WordDatabaseManager.remove(db, id_word);
    }

    public static void updateWord(SQLiteDatabase db, int id_word, int id_category, String picture, String text) {
        MainActivity.categories.get(id_category).getWords().get(id_word).setTitle(text);
        MainActivity.categories.get(id_category).getWords().get(id_word).setImage(picture);
        WordDatabaseManager.update(db, id_word, picture);
        WordTranslationDatabaseManager.update(db, id_word, "es", text);
    }
}
