package com.aphasiahelper.jtris.aphasiahelper.Word;

import java.util.Calendar;

/**
 * Created by jtris on 24/04/17.
 */

public class Word {
    private String image;
    private String title;
    private int id;
    private double morningMultiplier;
    private double middayMultiplier;
    private double afternoonMultiplier;
    private double eveningMultiplier;
    private double nightMultiplier;
    private int score;
    private int id_category;

    public Word(String image, String title, int id, int id_category) {
        this.image = image;
        this.title = title;
        this.id = id;
        this.id_category = id_category;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getScore() {
        double multiplier = 0;
        double gamma = 0.5;
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        if (hour <= 0 && hour < 6)
            multiplier = nightMultiplier;
        else if (hour < 12)
            multiplier = morningMultiplier;
        else if (hour < 15)
            multiplier = middayMultiplier;
        else if (hour < 19)
            multiplier = afternoonMultiplier;
        else if (hour < 24)
            multiplier = eveningMultiplier;


        return gamma * score + (1 - gamma) * multiplier;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void updateScore() {
        score++;
    }

    public void updateMultiplier() {
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);

        if (hour <= 0 && hour < 6)
            nightMultiplier += 1;
        else if (hour < 12)
            morningMultiplier += 1;
        else if (hour < 15)
            middayMultiplier += 1;
        else if (hour < 19)
            afternoonMultiplier += 1;
        else if (hour < 24)
            eveningMultiplier += 1;

    }

    public double getNightMultiplier() {
        return nightMultiplier;
    }

    public void setNightMultiplier(double nightMultiplier) {
        this.nightMultiplier = nightMultiplier;
    }

    public double getEveningMultiplier() {
        return eveningMultiplier;
    }

    public void setEveningMultiplier(double eveningMultiplier) {
        this.eveningMultiplier = eveningMultiplier;
    }

    public double getAfternoonMultiplier() {
        return afternoonMultiplier;
    }

    public void setAfternoonMultiplier(double afternoonMultiplier) {
        this.afternoonMultiplier = afternoonMultiplier;
    }

    public double getMiddayMultiplier() {
        return middayMultiplier;
    }

    public void setMiddayMultiplier(double middayMultiplier) {
        this.middayMultiplier = middayMultiplier;
    }

    public double getMorningMultiplier() {
        return morningMultiplier;
    }

    public void setMorningMultiplier(double morningMultiplier) {
        this.morningMultiplier = morningMultiplier;
    }


    public int getId_category() {
        return id_category;
    }

    public void setId_category(int id_category) {
        this.id_category = id_category;
    }

}
