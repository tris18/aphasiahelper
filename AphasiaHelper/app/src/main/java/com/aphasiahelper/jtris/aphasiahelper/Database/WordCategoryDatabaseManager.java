package com.aphasiahelper.jtris.aphasiahelper.Database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aphasiahelper.jtris.aphasiahelper.Word.Word;

import java.util.ArrayList;

/**
 * Created by javit_000 on 25/04/2017.
 */

public class WordCategoryDatabaseManager {
    private static final String TABLE_NAME = "word_category";
    private static final String CN_ID_CATEGORY = "id_category";
    private static final String CN_ID_WORD = "id_word";
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + CN_ID_CATEGORY + " int ," + CN_ID_WORD + " int NOT NULL, PRIMARY KEY (" + CN_ID_CATEGORY + "," + CN_ID_WORD + "), FOREIGN KEY (" + CN_ID_CATEGORY + ") REFERENCES " + CategoryDatabaseManager.TABLE_NAME + "(" + CategoryDatabaseManager.CN_ID + "),FOREIGN KEY (" + CN_ID_WORD + ") REFERENCES " + WordDatabaseManager.TABLE_NAME + "(" + WordDatabaseManager.CN_ID + "));";


    public static int insertValue(SQLiteDatabase db, int id_category, int id_word) {
        ContentValues values = new ContentValues();
        values.put(CN_ID_CATEGORY, id_category);
        values.put(CN_ID_WORD, id_word);
        return (int) db.insert(TABLE_NAME, null, values);
    }

    public static ArrayList<Word> getWordsByCategory(SQLiteDatabase db, int id_category) {
        ArrayList<Word> list = new ArrayList<>();
        Cursor c = db.rawQuery("select " + CN_ID_WORD + " from " + TABLE_NAME + " where " + CN_ID_CATEGORY + "=?", new String[]{Integer.toString(id_category)});
        if (c.moveToFirst())
            do {
                if (c.getInt(0) != -1)
                    list.add(new Word(null, null, c.getInt(0), id_category));
            } while (c.moveToNext());
        c.close();
        return list;
    }

    public static void removeCategory(SQLiteDatabase db, int id) {
        String[] args = new String[]{Integer.toString(id)};
        db.delete(TABLE_NAME, CN_ID_CATEGORY + "=?", args);

    }

    public static void removeWord(SQLiteDatabase db, int id) {
        String[] args = new String[]{Integer.toString(id)};
        db.delete(TABLE_NAME, CN_ID_WORD + "=?", args);
    }
}
