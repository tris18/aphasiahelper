package com.aphasiahelper.jtris.aphasiahelper.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.aphasiahelper.jtris.aphasiahelper.Category.Category;
import com.aphasiahelper.jtris.aphasiahelper.Database.CategoryDatabaseManager;
import com.aphasiahelper.jtris.aphasiahelper.Database.CategoryScoreDatabaseManager;
import com.aphasiahelper.jtris.aphasiahelper.Database.CategoryTranslationDatabaseManager;
import com.aphasiahelper.jtris.aphasiahelper.Database.WordCategoryDatabaseManager;
import com.aphasiahelper.jtris.aphasiahelper.Database.WordDatabaseManager;
import com.aphasiahelper.jtris.aphasiahelper.Database.WordScoreDatabaseManager;
import com.aphasiahelper.jtris.aphasiahelper.Database.WordTranslationDatabaseManager;
import com.aphasiahelper.jtris.aphasiahelper.MainActivity;
import com.aphasiahelper.jtris.aphasiahelper.R;
import com.aphasiahelper.jtris.aphasiahelper.Word.Word;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by javit_000 on 25/04/2017.
 */

public class Utils {


    public static void setImage(ImageView imageView, String filename, Context context) {

        int drawable = context.getResources().getIdentifier(filename, "drawable", context.getPackageName());
        if (drawable != 0)
            Picasso.get().load(drawable).fit().centerCrop().into(imageView);
        else {
            File file = context.getFileStreamPath(filename);
            if (file.exists())
                Picasso.get().load(file).fit().centerCrop().into(imageView);
            else
                Picasso.get().load(R.mipmap.ic_launcher).fit().centerCrop().into(imageView);
        }
    }

    public static void storeImage(String fileName, Bitmap image, Context context) {

        final FileOutputStream fos;
        try {
            fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();

        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }
}

